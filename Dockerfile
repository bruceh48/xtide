FROM ub18nginxphp7.2:latest


RUN apt-get update && apt-get install -y xtide && \
rm -rf /var/lib/apt/lists/*

COPY ./include/xtide.conf /etc
COPY ./include/usr/share/* /usr/share/xtide/


